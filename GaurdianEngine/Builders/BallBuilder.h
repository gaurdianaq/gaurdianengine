#ifndef _BALL_BUILDER_HG
#define _BALL_BUILDER_HG

#include "../Core/Builder.h"

class BallBuilder : public Builder
{
public:
	void Build(iMessageData* data);
};

#endif