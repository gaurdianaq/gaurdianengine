#ifndef _SYSTEM_TYPES_HG
#define _SYSTEM_TYPES_HG

#include "../Systems/BuilderSystem.h"
#include "../Systems/AudioSystems/AudioMessageSystem.h"
#include "../Systems/AudioSystems/AudioSystem.h"
#include "../Systems/InputSystems/DebugInput.h"
#include "../Systems/PhysicsSystems/CollisionResponseSystem.h"
#include "../Systems/PhysicsSystems/CollisionSystem.h"
#include "../Systems/PhysicsSystems/MotionMessageSystem.h"
#include "../Systems/PhysicsSystems/MotionSystem.h"
#include "../Systems/RenderSystems/LightSystem.h"
#include "../Systems/RenderSystems/MeshRenderSystem.h"
#include "../Systems/RenderSystems/CameraSystem.h"
#include "../Systems/RenderSystems/DebugRenderSystem.h"

#endif