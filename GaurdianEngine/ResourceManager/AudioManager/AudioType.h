#ifndef _AUDIO_TYPE_HG
#define _AUDIO_TYPE_HG

enum class AudioType { Sample, Stream, NetStream, Recording };

#endif