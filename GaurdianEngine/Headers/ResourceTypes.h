#ifndef _RESOURCE_TYPES_HG
#define _RESOURCE_TYPES_HG

#include "../ResourceManager/MeshManager/Mesh.h"
#include "../ResourceManager/ShaderManager/Shader.h"
#include "../ResourceManager/AudioManager/Audio.h"
#include "../ResourceManager/TextureManager/Texture.h"

#endif