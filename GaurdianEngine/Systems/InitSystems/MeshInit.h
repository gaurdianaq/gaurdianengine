#ifndef _MESH_INIT_HG
#define _MESH_INIT_HG

#include "../../Core/Engine.h"

class MeshInit : public Service
{
public:
	void Process(Entity* entity);
};

#endif // !_MESH_INIT_HG
